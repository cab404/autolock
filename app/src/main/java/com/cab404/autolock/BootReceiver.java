package com.cab404.autolock;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * This one autostarts everything if it receives some boot
 */
public class BootReceiver extends BroadcastReceiver {

    // We don't care who tries to start lock service, the earlier the better :D
    @SuppressLint({"UnsafeProtectedBroadcastReceiver"})
    @Override
    public void onReceive(Context context, Intent intent) {
        // wow, android 8 does weird things to your on boot methods
        Utils.tryStart(context);
    }
}
