package com.cab404.autolock;

import android.app.admin.DevicePolicyManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.cab404.autolock.lock.LockJobService;
import com.cab404.autolock.lock.LockService;

import java.io.File;
import java.io.IOException;

import static android.content.Context.DEVICE_POLICY_SERVICE;

/**
 * Some useful methods
 */
public class Utils {

    // We don't really need overhead of using preferences, so...
    public static boolean isAutolockEnabled(Context context) {
        return new File(context.getFilesDir(), ".lockdown").exists();
    }

    /**
     * Returns whether we successfully enabled or disabled autolock
     */
    public static boolean setAutolockEnabled(Context context, boolean value) {
        File flag = new File(context.getFilesDir(), ".lockdown");
        if (value)
            try {
                return flag.exists() || flag.createNewFile();
            } catch (IOException e) {
                return false;
            }
        else
            return !flag.exists() || flag.delete();
    }


    /**
     * Checks whether our admin is active.
     */
    public static boolean isAdminActive(Context context) {
        return ((DevicePolicyManager) context.getSystemService(DEVICE_POLICY_SERVICE))
                .isAdminActive(new ComponentName(context, AdminReceiver.class));
    }

    /**
     * Whether we can and want to lockdown
     */
    public static boolean willLockdown(Context context) {
        return Utils.isAdminActive(context) && Utils.isAutolockEnabled(context);
    }

    /**
     * Starts lock service one or the other way. Boot compatible.
     */
    public static void tryStart(Context context) {
        if (willLockdown(context)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE)).schedule(
                        new JobInfo.Builder(1, new ComponentName(context, LockJobService.class))
                                // no u will be my database, beeeeaaaaatch
                                .setOverrideDeadline(0)
                                .setPersisted(true)
                                .build()
                );
            } else
                context.startService(new Intent(context, LockService.class));
        }
    }

    /**
     * Tries to stop service (if this is necessary)
     */
    public static void tryStop(Context context) {
        if (!willLockdown(context)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // I like small apps :з
                ((JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE))
                        .cancelAll();
            } else
                context.stopService(new Intent(context, LockService.class));
        }
    }
}
