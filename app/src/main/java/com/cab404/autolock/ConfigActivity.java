package com.cab404.autolock;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Configuration activity.
 */
public class ConfigActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.tryStart(this);

        // If admin is not active, then we'll throw user into admin activation screen
        if (!Utils.isAdminActive(this)) {
            Intent add = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            add.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, new ComponentName(this, AdminReceiver.class));
            add.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getString(R.string.activate_admin_prompt));
            startActivity(add);
            finish();
            return;
        }

        recreateView();
    }

    void recreateView() {

        LinearLayout conf = new LinearLayout(this);
        {
            conf.setOrientation(LinearLayout.VERTICAL);
            conf.setGravity(Gravity.CENTER_HORIZONTAL);

            float dp = getResources().getDisplayMetrics().density;
            int standardPadding = (int) (8 * dp);
            conf.setPadding(
                    standardPadding * 2,
                    standardPadding * 2,
                    standardPadding * 2,
                    standardPadding * 2
            );

            // Mode switch
            Switch sw = new Switch(this);
            {
                sw.setPadding(
                        standardPadding,
                        standardPadding,
                        standardPadding,
                        standardPadding
                );
                sw.setText(R.string.should_lockdown);
                conf.addView(sw);
                sw.setChecked(Utils.isAutolockEnabled(this));
                sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        boolean success = setActive(isChecked);
                        if (!success)
                            buttonView.setChecked(Utils.isAutolockEnabled(buttonView.getContext()));
                    }
                });
            }

            // Simple description
            TextView text = new TextView(this);
            {
                text.setPadding(
                        standardPadding,
                        standardPadding,
                        standardPadding,
                        standardPadding
                );
                text.setText(R.string.lockdown_desc);
                conf.addView(text);
            }
        }

        setContentView(conf);

    }

    boolean setActive(boolean active) {
        boolean success = Utils.setAutolockEnabled(this, active);
        if (success)
            if (active)
                Utils.tryStart(this);
            else
                Utils.tryStop(this);
        return success;
    }

}
