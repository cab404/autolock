package com.cab404.autolock.lock;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;

import com.cab404.autolock.ConfigActivity;
import com.cab404.autolock.R;
import com.cab404.autolock.Utils;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Handles all the logic of locking.
 */
public class LockDelegate {
    private static final String CHANNEL_ID = "this_is_a_good_channel_id";
    private Service service;
    private LockReceiver lockReceiver = new LockReceiver();
    private NotificationChannel channel = null;


    public LockDelegate(Service service) {
        this.service = service;
    }

    public void createChannel() {
        if (channel != null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel(
                    CHANNEL_ID,
                    service.getString(R.string.app_name),
                    // this somehow doesn't work on lineageOS :(
                    NotificationManager.IMPORTANCE_MIN
            );
            channel.enableVibration(false);
            channel.enableLights(false);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            ((NotificationManager) service.getSystemService(NOTIFICATION_SERVICE))
                    .createNotificationChannel(channel);
        }
    }

    private boolean isScreenOn() {
        PowerManager man = (PowerManager) service.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH)
            return man.isInteractive();
        else
            // might yield 'false' if user is currently talking on phone and screen is off due to proximity sensor
            // though since this method is only used during startup, I don't think that would be a huge ux problem
            return man.isScreenOn();
    }

    public boolean start() {
        // We don't need to start service if we can't do anything with it, are we?
        if (!Utils.willLockdown(service))
            return false;

        createChannel();

        // fy android and your fn support lib, you 50 Mb gs-licking data hoarding binary bastards
        Notification.Builder builder = new Notification.Builder(service);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
            builder.setContentText(service.getString(R.string.hide_notif_help));
        }

        Notification notification = builder
                .setSmallIcon(R.drawable.ic_lock)
                .setContentTitle("AutoLock is on")
                .setContentIntent(
                        PendingIntent.getActivity(
                                service,
                                0,
                                new Intent(service, ConfigActivity.class),
                                0
                        )
                )
                .build();


        service.startForeground(R.id.notification, notification);
        service.registerReceiver(lockReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        // and now we'll check whether screen is already off, and engage lockdown, if service was activated while screen is off.
        if (!isScreenOn())
            Lock.lock(service);

        return true;
    }

    public void destroy() {
        service.unregisterReceiver(lockReceiver);
    }

}
