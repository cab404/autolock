package com.cab404.autolock.lock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * For pre-lollipop, plain and simple services.
 */
public class LockService extends Service {

    private LockDelegate l = new LockDelegate(this);

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (l.start())
            return START_STICKY;
        else
            return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        l.destroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
