package com.cab404.autolock.lock;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.cab404.autolock.Utils;

/**
 * Post-lollipop, tee-hee, non-serviceable devices
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LockJobService extends JobService {

    // I really miss kotlin :(
    private LockDelegate l = new LockDelegate(this);

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.v("mowmow", "Started lock service through job");
        return l.start();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        l.destroy();
        // I think that toasting user about potential vulnerability is a good idea.
        Toast.makeText(this, "AutoLock is off!", Toast.LENGTH_LONG).show();
        Utils.tryStart(this);
        return Utils.willLockdown(this);
    }


}
