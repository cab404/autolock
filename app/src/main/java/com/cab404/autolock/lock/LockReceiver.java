package com.cab404.autolock.lock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

/**
 * This one receives broadcast when screen is switched off
 */
public class LockReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // I included checks earlier, but I think they are pretty much not needed, so I removed them.


        // Initiating lockdown!
        if (Lock.lock(context))
            // Notifying that lockdown is indeed engaged
            ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(new long[]{30, 50, 30, 50}, -1);

    }
}
