package com.cab404.autolock.lock;

import android.app.admin.DevicePolicyManager;
import android.content.Context;

import com.cab404.autolock.Utils;

public class Lock {
    static boolean lock(Context context) {
        boolean shouldLock = Utils.willLockdown(context);
        if (shouldLock) {
            DevicePolicyManager dpman = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
            dpman.lockNow();
        }
        return shouldLock;
    }
}
