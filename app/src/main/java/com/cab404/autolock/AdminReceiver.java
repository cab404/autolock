package com.cab404.autolock;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * We need this thing just to look at it. Isn't it pretty in it's simplicity?
 */
public class AdminReceiver extends DeviceAdminReceiver {
    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        context.startActivity(
                new Intent(context, ConfigActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        );
    }
}
