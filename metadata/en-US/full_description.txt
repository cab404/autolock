<font color="red">•</font> Are you afraid that someone will use your biometrics forcefully to get to your data? (and you are ready to withstand torture instead)
    <font color="orange">•</font> Do your roommate(s) stalk on you and try every single way to install tracker on your phone?
    <font color="yellow">•</font> Do you still want to use fingerprint in your favourite banking app, because you don't want to use 4-digit wannabe password every few minutes in front of bunch of people?
    <font color="green">•</font> Have you ever screamed in agony, while trying to find nonexistent option to disable fingerprint sensor on unlock screen?
    
    Then this app is for you!
    It engages lockdown mode every time your screen switches off, and you can only unlock it with a password.

Licensed under GPLv3. Source code is available here: https://gitlab.com/cab404/autolock